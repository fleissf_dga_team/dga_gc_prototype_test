# FLEISS Software Foundation

# DGA MMORPG Game Client Prototype

## Release Notes

### Version 0.2
22 January 2019

The version 0.2 contains multiple corrections in naming of interfaces, classes, and models in accordance with the General Naming Conventions of the DGA MMORPG Project.

### Version 0.1
19 September 2018

The version 0.1 is intended to test capability of the jMonkeyEngine game engine to carry out the DGA game client with acceptable productivity on different operating systems without any optimization. This version includes the game client's feature-complete core object model providing basic game functionality.
