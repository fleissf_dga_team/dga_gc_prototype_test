# FLEISS Software Foundation

# DGA MMORPG

DGA is a free/libre and open-source, cross-platform massively multiplayer online role-playing video game featuring weapons. The first type of weapons being realized in DGA is land combat vehicles. DGA is designed to be cross-platform by used technologies and platforms, however, the DGA project aims at providing a full-featured MMORPG on Linux operating system.

## DGA Game Client Prototype, Feature-Complete Version

DGA Game Client Prototype is intended to model, test, and work out the DGA game client's functions and modes running within the jMonkeyEngine game engine only.

## Download & Build Instructions

The outer repository of DGA Game Client Prototype, Feature-Complete Version, contains build-ready projects in the form of Git submodules (inner repositories).

Every project (build inner repository) holds configuration and resource files for a specific Integrated Development Environment (IDE) or build system, for example, jMonkeyEngine SDK.

The source codes and models are disposed in its proper submodule (sources inner repository) located within the project.

Hereby the structure of repositories looks as follows:

* [dga_gc_prototype_rel](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/) - Outer repository.

    * [dga_jme_gc_prototype_rel](https://bitbucket.org/fleissf_dga_team/dga_jme_gc_prototype_rel/src/master/) - Build inner repository containing a project for jMonkeyEngine SDK of the latest stable version (the current is v.3.2.0).

        * [dga_gc_prototype_sources_rel](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_sources_rel/src/master/) - Sources inner repository containing source codes, models, and other source files.

### Download Instructions

You can download the repositories by the following ways:

* by a Command-Line Interface (CLI), for example, Bash shell on Linux, however, be assured that a Git version-control system is already installed and properly configured in your operating system.

* by a Git GUI Client of your choice; you can find a list of recommended clients at [the Git website](https://git-scm.com/downloads/guis/).

#### Download with CLI

In a CLI accomplish the following commands (an example for Bash shell) to download all repositories at once:

```
$ cd /home/temp/
$ git clone --recursive https://fleissf@bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel.git
```

Otherwise you can download repositories separately one by one:

```
$ cd /home/temp/
$ git clone https://fleissf@bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel.git
$ cd dga_gc_prototype_rel/
$ git submodule init
$ git submodule update
$ cd dga_jme_gc_prototype_rel/
$ git submodule init
$ git submodule update
```

#### Download with Git GUI Client

For downloading the repositories with a Git GUI Client you can use, for example:

* [SmartGit](https://www.syntevo.com/smartgit/) is a cross-platform Git client written in Java and free for non-commercial use on macOS, Windows and Linux.

* [TortoiseGit](https://tortoisegit.org/) is a free/libre Git client for Windows.

* [Git Extensions](https://gitextensions.github.io/) is another free/libre Git client for Linux, macOS, and Windows.

Every client has its proper specialities but they all are based on the common system of Git, and the logic of submodules is always similar: you download the outer repository and then initialize and pull (update) every submodule (subrepository) apart.

After all, in the target directory you have got the repositories structured as shown above.

### Build Instructions

The most simple way for building DGA Game Client Prototype from source codes is to use IDEs and build systems with which the prototype has been developed and built. Otherwise you can use only the sources inner repository containing all source materials and build the program with some IDE or build system of your choice.

#### Build **dga_jme_gc_prototype_rel**

The build inner repository [dga_jme_gc_prototype_rel](https://bitbucket.org/fleissf_dga_team/dga_jme_gc_prototype_rel/src/master/) contains a fully configured project for jMonkeyEngine SDK which is an IDE based on NetBeans IDE intended to simplify developing 3D Applications with the jMonkeyEngine game engine. jMonkeyEngine SDK contains everything ever needed to develop a full 3D application. 

The SDK is open-source as well as jMonkeyEngine. You can freely download the SDK on [the release page](https://github.com/jMonkeyEngine/sdk/releases) of the project (please, choose a stable version of the SDK corresponding to the version of your operating system).

After having downloaded and installed the SDK you simply need to open the build inner repository as a project (menu File > Open Project...), build and run it (click the project name with the right mouse button and choose 'Build' in the opened menu; after the project has been finished to be built choose 'Run' in the same menu to run the program).

If you want to not only recompile the prototype project but rebuild the application launchers for different operating systems, you have to change the project's configuration. Click the project name with the right mouse button and choose 'Properties' in the opened menu, then in the section 'Application' choose the subsection 'Desktop' and set flags as shown on the next image.

![jME SDK Desktop Properties Image](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/wiki/images/jme-sdk-desktop-properties.png)

The image shows the most complete configuration with a bundled Java Runtime Environment (JRE). You have to specify the version of used JRE. After you save the project configuration - click the button 'OK' in the Project Properties window, - the IDE creates in the project directory a folder called 'resources' with a subfolder 'desktop-deployment' - in this subfolder you have to place the target JRE's distributive files in the format of gzipped TAR (tar.gz). Now you rebuild the project and get in the 'dist' folder the prototype's application launchers for Linux, macOS, and Windows.

For more detailed instructions on how to manage, build, and run projects please consult the following guides:

* NetBeans IDE's [Documentation, Training & Support page](https://netbeans.org/kb/index.html) and particularly [Java Quick Start Tutorial](https://netbeans.org/kb/docs/java/quickstart.html) or [Creating, Importing, and Configuring Java Projects](https://netbeans.org/kb/73/java/project-setup.html#deploy).

* jMonkeyEngine SDK's [Documentation page](https://wiki.jmonkeyengine.org/) and particularly guides on [Creating Projects](https://wiki.jmonkeyengine.org/sdk/project_creation.html) and [Application Deployment](https://wiki.jmonkeyengine.org/sdk/application_deployment.html).
