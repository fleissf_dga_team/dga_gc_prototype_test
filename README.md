# FLEISS Software Foundation

# DGA MMORPG

DGA is a free/libre and open-source, cross-platform massively multiplayer online role-playing video game featuring weapons. The first type of weapons being realized in DGA is land combat vehicles. DGA is designed to be cross-platform by used technologies and platforms, however, the DGA project aims at providing a full-featured MMORPG on Linux operating system.

## DGA Game Client Prototype, Feature-Complete Version

DGA Game Client Prototype is intended to model, test, and work out the DGA game client's functions and modes running within the jMonkeyEngine game engine only.

### Getting Started

This repository is the outer repository of DGA Game Client Prototype, Feature-Complete Version, and it contains build-ready projects in the form of Git submodules (inner repositories). Every project (build inner repository) holds configuration and resource files for a specific Integrated Development Environment (IDE) or build system. The source codes and models are disposed in a separate submodule (sources inner repository) located within the project.

Hereby the full structure of repositories looks as follows:

* [dga_gc_prototype_rel](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/) - Outer repository.

    * [dga_jme_gc_prototype_rel](https://bitbucket.org/fleissf_dga_team/dga_jme_gc_prototype_rel/src/master/) - Project for jMonkeyEngine SDK of the latest stable version (the current is v.3.2.0).

        * [dga_gc_prototype_sources_rel](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_sources_rel/src/master/) - Repository for source codes and models.        

#### Prerequisites & Installing

You can find binary builds of DGA Game Client Prototype on [the downloads page](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/downloads/). Choose a build corresponding to the version of your operating system. Every build includes all necessary components and does not need of any external program. Only the JAR build requires of an external Java Runtime Environment (JRE) that should be already installed in your operating system to run the program. For detailed instructions on how to install and run DGA Game Client Prototype please see [the install guide](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/INSTALL.md); for instructions on how to build the Prototype from source codes please see [the build guide](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/BUILD.md).

### Technologies

DGA Game Client Prototype is based on jMonkeyEngine version 3 that is a free, open source game engine written in Java and using the Lightweight Java Game Library (LWJGL) as its default renderer. jMonkeyEngine is a community-centric open source project released under [the New BSD 3-clause License](https://opensource.org/licenses/BSD-3-Clause). You can find additional information about jMonkeyEngine and freely download it as well as SDK at [http://jmonkeyengine.org](http://jmonkeyengine.org).

### Documentation

Brief answers are in md files here; detailed information please see on [the wiki pages](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/wiki/).

### Contributing

On how to contribute to our project please see [the contributing guide](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/CONTRIBUTING.md) and also pay attention to [our code of conduct](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/CODE_OF_CONDUCT.md).

### Versioning

Please see detailed information about every version in [the release notes](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/RELEASE_NOTES.md).

### License

DGA Game Client Prototype is part of the FLEISS Software Foundation's projects and is distributed under the terms of the GNU General Public License version 3 or any later version. You can use, redistribute and/or modify it under the terms of the GNU General Public License version 3 or any later version. Please, see [the license](https://bitbucket.org/fleissf_dga_team/dga_gc_prototype_rel/src/master/LICENSE.md) for details.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

### References

#### jMonkeyEngine (jME)

Copyright (c) 2009-2019 jMonkeyEngine. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of 'jMonkeyEngine' nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
